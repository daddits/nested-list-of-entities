<?php

/**
 * Class DBConnector
 */
class DBConnector
{
    private static $instance = null;
    private $conn;
    private $host = 'localhost';
    private $user = 'root';
    private $pass = 'rootpasswordgiven';
    private $name = 'infinity';

    /**
     * DBConnector constructor
     */
    private function __construct()
    {
        try {
            $this->conn = new \PDO("mysql:host={$this->host};dbname={$this->name}", $this->user, $this->pass, [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"]);
        } catch (PDOException $e) {
            die($e->getMessage());
        }

    }

    /**
     * @return DBConnector|null
     */
    public static function getInstance(): DBConnector
    {
        if (!self::$instance) {
            self::$instance = new DBConnector();
        }

        return self::$instance;
    }

    /**
     * @param string $sql
     * @return false|PDOStatement
     */
    public function query(string $sql)
    {
        return $this->conn->query($sql);
    }

    /**
     * @param string $query
     * @return bool|PDOStatement
     */
    public function prepare(string $query)
    {
        return $this->conn->prepare($query);
    }
}