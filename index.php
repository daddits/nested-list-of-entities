<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

spl_autoload_register(function ($class) {
    include $class . '.php';
});
$db = DBConnector::getInstance();
$sql = 'SELECT * FROM categories ORDER BY name';
try {
    $sth = $db->prepare($sql);
    $sth->execute();
    $allCategories = $sth->fetchAll(PDO::FETCH_ASSOC);
} catch (PDOException $e) {
    die($e->getMessage());
}
$categories = [
    'nested' => [],
    'parents' => []
];
/**
 * populate $categories array with sorted data (depends on value of parent_category_id)
 */
foreach ($allCategories as $item) {
    $categories['nested'][$item['id']] = $item;
    $categories['parents'][$item['parent_category_id']][] = $item['id'];
}

/**
 * @param int $parent_id
 * @param array $categories
 * @return string
 */
function buildNestedList(int $parent_id, array $categories)
{
    $list = "";
    if (isset($categories['parents'][$parent_id])) {
        foreach ($categories['parents'][$parent_id] as $category_id) {
            if (!isset($categories['parents'][$category_id])) {
                $list .= '<li>' . $categories['nested'][$category_id]['name'] . '</li>';
            } else {
                $list .= '<li>' . $categories['nested'][$category_id]['name'] . '<ul>';
                $list .= buildNestedList($category_id, $categories);
            }
        }
        $list .= '</ul></li>';
    }
    return $list;
}

echo buildNestedList(0, $categories);
